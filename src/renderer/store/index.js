import Vue from 'vue'
import Vuex from 'vuex'

import { createPersistedState } from 'vuex-electron'
Vue.use(Vuex)

export default new Vuex.Store({
  namespaced: true,
  plugins: [
    createPersistedState()
    // createSharedMutations()
  ],
  strict: process.env.NODE_ENV !== 'production',
  state: {
    matrix: [],
    matrixAnswer: [],
    n: 5
  },
  mutations: {
    addMa: (state, value) => {
      console.log('vaue', value)
      state.matrix = value
      console.log('state', state.matrix)
    },
    n: (state, value) => {
      state.n = value
    },
    addAn: (state, value) => {
      console.log('vale', value)
      state.matrixAnswer = value
      console.log('stae', state.matrixAnswer)
    }
  },
  actions: {
    addMatrix ({commit}, payload) {
      commit('addMa', payload)
    },
    addN ({commit}, payload) {
      commit('n', payload)
    },
    addAnswerMatrix ({commit}, payload) {
      commit('addAn', payload)
    }
  },
  getters: {
    matrix (state) {
      return state.matrix
    },
    n (state) {
      return state.n
    },
    matrixAnswer (state) {
      return state.matrixAnswer
    }
  }
})
